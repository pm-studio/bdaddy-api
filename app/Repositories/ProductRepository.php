<?php

namespace App\Repositories;

use Foolz\SphinxQL\Drivers\Pdo\Connection;
use Foolz\SphinxQL\Facet;
use Foolz\SphinxQL\SphinxQL;

class ProductRepository
{
    public function getProductById($product_id) {
        $product = \DB::table('products')->where('products.id', '=', $product_id)
            ->leftJoin('isbns', 'products.id','=', 'isbns.product_id')
            ->limit(1)
            ->get([
                'products.author as author',
                'products.name as name',
                'products.page_extent as page_extent',
                'products.year as year',
                'products.description as description',
                'isbns.isbn_full as isbn',
            ])
            ->toArray()[0];

        $description = '';
        $descriptionStrings = explode(PHP_EOL, $product->description);
        foreach ($descriptionStrings as $item) {
            $description .= $item . "<br>";
        }
        $result['description'] = $description;

        $result['id'] = $product_id;
        $result['author'] = $product->author;
        $result['name'] = htmlspecialchars_decode($product->name);
        $result['page_extent'] = $product->page_extent;
        $result['year'] = $product->year;
        $result['isbn'] = $product->isbn;

        $image = \DB::table('pictures')
            ->where('pictures.product_id', '=', $product_id)
            ->leftJoin('advcampaigns', 'pictures.advcampaign_id','=', 'advcampaigns.id')
            ->orderByDesc('advcampaigns.picture_priority')
            ->limit(1)
            ->get([
                'pictures.url as image',
            ])
            ->toArray();

        $result['image'] = ($image) ? $image[0]->image : '';

        $result['offers'] = \DB::table('offers')->where('product_id', '=', $product_id)
            ->leftJoin('advcampaigns', 'advcampaigns.id','=', 'offers.advcampaign_id')
            ->whereNotIn('advcampaign_id', [14514,17712])
            ->orderBy('offers.price')
            ->get([
                'offers.id as id',
                'offers.price as price',
                'offers.available as available',
                'offers.format as format',
                'offers.downloadable as downloadable',
                'advcampaigns.name as advcampaign'
            ])
            ->toArray();

        return $result;
    }

    public function getOfferById ($id) {
        return \DB::table('offers')
            ->where('id', '=', intval($id))
            ->limit(1)
            ->get([
                'url'
            ])
            ->toArray()[0];
    }

    public function getProductPager($page, $search) {
        $limit = 50;
        $offset = ($page - 1) * $limit;

        if (!$search) return ['data' => []];

        $conn = new Connection();
        $conn->setParams([
            'host' => env('SPHINX_HOST'),
            'port' => env('SPHINX_PORT'),
        ]);

        $result = (new SphinxQL($conn))
            ->select()
            ->from('BookdaddyProductsIndex')
            ->match(['name','author'], "$search")
            ->option('field_weights', ['name' => 20, 'author' => 10])
            ->option('index_weights', ['year' => 100])
            ->option('ranker', 'bm25')
            ->limit($limit)
            ->offset($offset)
            ->facet((new Facet($conn))
                ->facet(array('year'))
                ->orderBy('COUNT(*)', 'DESC'))
            ->facet((new Facet($conn))
                ->facet(array('author'))
                ->orderBy('COUNT(*)', 'DESC'))
            ->facet((new Facet($conn))
                ->facetFunction('INTERVAL', ['year', 2010, 2020])
                ->orderByFunction('FACET', '', 'ASC')
            )
            ->executeBatch();

        $records = [];
        $years = [];
        $product_count = 0;

        for ($x=0; $x<$result->count(); $x++) {

            switch ($x) {
                case 0:
                    $records = $result->getNext()->fetchAllAssoc();
                    break;
                case 1:
                    $years = $result->getNext()->fetchAllAssoc();
                    foreach ($years as $year)
                        $product_count += $year["count(*)"];
                    break;
            }
        }

        $last_page = intval(ceil($product_count / $limit));
        $offset = ($page - 1) * $limit;
        $to = $offset + $limit;

        $data = [
            "current_page" => $page,
            "data" => $records,
            "from" => $offset + 1,
            "last_page" => $last_page,
            "per_page" => $limit,
            "to" => ($page >= $last_page) ? $product_count : $to,
            "total" => $product_count,
        ];

        if ($page > $last_page) {
            $data = ['data' => []];
        }
        return $data;
    }
}
