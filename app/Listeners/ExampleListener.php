<?php

namespace App\Listeners;

use App\Events\ExampleEvent;
use App\Repositories\ProductRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ExampleListener
{

    protected $repository;

    /**
     * Create the event listener.
     *
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ExampleEvent  $event
     * @return void
     */
    public function handle(ExampleEvent $event)
    {
        //
    }
}
