<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function product(Request $request)
    {
        $product_id = intval($request->get("id"));
        return json_encode($this->repository->getProductById($product_id), JSON_HEX_TAG);
    }

    public function iproduct(Request $request)
    {
        $page = ($request->get("page")) ? intval($request->get("page")) : 1;
        $search = $request->get("search");
        return $this->repository->getProductPager($page, $search);
    }

    public function popular() {
        try {
            return \Storage::disk('local')->get('popular.json');
        } catch (FileNotFoundException $e) {
            return '';
        }
    }

    public function latest() {
        try {
            return \Storage::disk('local')->get('latest.json');
        } catch (FileNotFoundException $e) {
            return '';
        }
    }

    public function offer($id) {
        $result = $this->repository->getOfferById($id);
        return redirect($result->url);
    }

}
