<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return '';
});

$router->get('/product', 'ProductController@product');
$router->get('/iproduct', 'ProductController@iproduct');
$router->get('/popular', 'ProductController@popular');
$router->get('/latest', 'ProductController@latest');
$router->get('/offer/{id}', 'ProductController@offer');
